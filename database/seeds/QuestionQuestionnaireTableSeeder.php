<?php

use Illuminate\Database\Seeder;

class QuestionQuestionnaireTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('question_questionnaire')->insert([
            ['questionnaire_id' => 1, 'question_id' => 1,],
            ['questionnaire_id' => 1, 'question_id' => 2,],
            ['questionnaire_id' => 1, 'question_id' => 3,],
            ['questionnaire_id' => 2, 'question_id' => 4,],
            ['questionnaire_id' => 2, 'question_id' => 5,],
            ['questionnaire_id' => 2, 'question_id' => 6,],
            ['questionnaire_id' => 3, 'question_id' => 7,],
            ['questionnaire_id' => 3, 'question_id' => 8,],
            ['questionnaire_id' => 3, 'question_id' => 9,],
        ]);
    }
}
