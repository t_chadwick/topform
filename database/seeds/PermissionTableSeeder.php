<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            ['id' => 1, 'name' => 'crud_users', 'label' => 'CRUD All Users'],
            ['id' => 2, 'name' => 'rud_questionnaires', 'label' => 'Manage all Questionnaires'],
            ['id' => 3, 'name' => 'crud_own_questionnaire', 'label' => 'CRUD Own Questionnaires.'],
            ['id' => 4, 'name' => 'admin_access', 'label' => 'Full admin access'],
        ]);
    }
}
