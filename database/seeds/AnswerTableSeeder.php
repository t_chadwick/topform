<?php

use Illuminate\Database\Seeder;

class AnswerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add available answers for the initial questions

        DB::table('answers')->insert([
            ['id' => 1, 'answer' => 'Male',],
            ['id' => 2, 'answer' => 'Female',],
            ['id' => 3, 'answer' => 'Below 18',],
            ['id' => 4, 'answer' => '18 - 25',],
            ['id' => 5, 'answer' => '26 +',],
            ['id' => 6, 'answer' => 'Yes',],
            ['id' => 7, 'answer' => 'No',],
            ['id' => 8, 'answer' => 'Yes',],
            ['id' => 9, 'answer' => 'No',],
            ['id' => 10, 'answer' => 'Yes',],
            ['id' => 11, 'answer' => 'No',],
            ['id' => 12, 'answer' => 'Yes',],
            ['id' => 13, 'answer' => 'No',],
            ['id' => 14, 'answer' => 'Yes',],
            ['id' => 15, 'answer' => 'No',],
            ['id' => 16, 'answer' => 'Yes',],
            ['id' => 17, 'answer' => 'No',],
            ['id' => 18, 'answer' => 'Everyday',],
            ['id' => 19, 'answer' => '2 - 3 Days per week',],
            ['id' => 20, 'answer' => 'Once per week',],
            ['id' => 21, 'answer' => 'Once per month',],
        ]);
    }
}
