<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add initial questions for 3 different questionnaires

        DB::table('questions')->insert([
            ['id' => 1, 'text' => 'Are you Male or Female?',],
            ['id' => 2, 'text' => 'How old are you?',],
            ['id' => 3, 'text' => 'Are you a student?',],
            ['id' => 4, 'text' => 'Do you watch Youtube videos?',],
            ['id' => 5, 'text' => 'Do you have a Youtube account?',],
            ['id' => 6, 'text' => 'Do you upload videos to Youtube?',],
            ['id' => 7, 'text' => 'Do you use Facebook?',],
            ['id' => 8, 'text' => 'Do you use Twitter?',],
            ['id' => 9, 'text' => 'How regularly do you interact with a social network?',],
        ]);
    }
}
