<?php

use Illuminate\Database\Seeder;

class QuestionnaireTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add 3 Initial Questionnaires

        DB::table('questionnaires')->insert([
            ['id' => 1, 'title' => 'Who are you?',],
            ['id' => 2, 'title' => 'Youtube Survey',],
            ['id' => 3, 'title' => 'Social Media',],
        ]);
    }
}
