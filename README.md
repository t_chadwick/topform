#Welcome to TopForm#

### Origin ###
This project was created as part of a module coursework during my second year of studying Web Design & Development. This repository is not the original, I have created this one to showcase my application.
### The Application ###
TopForm is an online Survey system, designed to allow registered users to create questionnaires and any other user to fill them in. As of right now this system is incomplete, it allows the following:

* User Registration & Login
* Admin Tasks including CRUD Users & RD Questionnaires.
* CRUD of Questionnaires by their authors.

The site currently fails at the point of submitting a filled in questionnaire. This has been identified as a database error that goes as far back as initial modelling. Steps are currently being taken to conclude this project. Depending on the circumstances this will either mean fixing the problem and refining the build. OR Refining the build as it is in this state and acknowledging within this Readme what the problem was and how it will be avoided in future projects.