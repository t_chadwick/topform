<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Answers</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>

<h1>Answers</h1>
<section>
    @if (isset ($answers))

        <table>
            <tr>
                <th>Answer</th>
                <th>Blank</th>
            </tr>
            @foreach ($answers as $answer)
                <tr>
                    <td><a href="/answers/{{ $answer->id }}/edit">{{ $answer->answer }}</a></td>
                </tr>
            @endforeach
        </table>
    @else
        <p>no answers</p>
    @endif
</section>

</body>
</html>