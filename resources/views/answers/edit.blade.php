<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit - Answer</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<h1>Edit Answer</h1>

<div class="row">
    {!! Form::model($answer, ['method' => 'PATCH', 'url' => '/answers/' . $answer->id]) !!}

    <div class="large-12 columns">
        {!! Form::label('answer', 'Answer: ') !!}
        {!! Form::text('answer', null) !!}
    </div>
    <div>
        {!! Form::hidden('id['.$id.']') !!}
    </div>
    <div class="large-12 columns"><p></p></div>
    <div class="large-12 columns">
        {!! Form::submit('Update Answer', ['class' => 'button round success']) !!}
    </div>
    {!! Form::close() !!}
    <div class="large-12 columns"><p></p></div>
    <div class="large-12 columns">
        {!! Form::open(['method' => 'DELETE', 'action' => ['AnswerController@destroy', $answer->id]]) !!}
            {!! Form::submit('Delete Answer', ['class' => 'button round small alert']) !!}
        {!! Form::close() !!}
    </div>
</div>
</body>
</html>