<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin - Questionnaires</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<!-- Add Admin Nav-->
    @can('admin_access')

        @include('admin/includes/adminnav')

    @endcan

    <h1>Questionnaires</h1>
<section>
    @if (isset ($questionnaires))

        <table>
            <tr>
                <th>Title</th>
                <th>Created</th>
            </tr>
            @foreach ($questionnaires as $questionnaire)
                <tr>
                    <td><a href="/admin/questionnaires/{{ $questionnaire->id }}/edit" name="{{ $questionnaire->title }}">{{ $questionnaire->title }}</a></td>
                    <td>{{ $questionnaire->created_at }}</td>
                </tr>
            @endforeach
        </table>
    @else
        <p>no questionnaires</p>
    @endif
</section>

</body>
</html>