<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin - Users</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<!-- Add Admin Nav-->
@can('admin_access')

@include('admin/includes/adminnav')

@endcan

<h1>Users</h1>
<a href="/admin/users/create"><button>Add User</button></a>
<section>
    @if (isset ($users))

        <table>
            <tr>
                <th>Username</th>
                <th>email</th>
                <th>Roles</th>
            </tr>
            @foreach ($users as $user)
                <tr>
                    <td><a href="/admin/users/{{ $user->id }}/edit" name="{{ $user->name }}">{{ $user->name }}</a></td>
                    <td> {{ $user->email }}</td>
                    <td>
                        <ul>
                            @foreach($user->roles as $role)
                                <li>{{ $role->label }}</li>
                            @endforeach
                        </ul>
                    </td>
                </tr>
            @endforeach
        </table>
    @else
        <p>no users</p>
    @endif
</section>

</body>
</html>