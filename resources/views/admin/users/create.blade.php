<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin - Questionnaires</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<!-- Add Admin Nav-->
@can('admin_access')

@include('admin/includes/adminnav')

@endcan

    <h1>Add User</h1>

@if ($errors->any())
    <div>
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

{!! Form::open(array('action' => 'UserController@store', 'id' => 'createuser')) !!}
{{  csrf_field() }}
<div class="row large-12 columns">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns">
    {!! Form::label('email', 'Email Address:') !!}
    {!! Form::text('email', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::text('password', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-4 columns">
    {!! Form::submit('Add User', ['class' => 'button']) !!}
</div>
{!! Form::close() !!}
</body>
</html>