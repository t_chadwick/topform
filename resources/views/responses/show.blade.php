<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Responses</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
    <h1>Responses - {{ $questionnaire->title }}</h1>
    <!--Commented out Code, used this to test the data retrieval-->
    {{--<div>
        <h2>Data</h2>
        <h2>Questionnaire</h2>{{ $questionnaire->title }}
        <h2>Questions</h2>
        @foreach($questionnaire->questions as $question)
            {{ $question->text }}
        @endforeach
        <h2>Responses</h2>
        @foreach($questionnaire->questions as $question)
                    {{ $question->responses}}
        @endforeach
        <h2>Responses</h2>
        @foreach($questionnaire->questions as $question)
            @foreach($question->responses as $response)
                @foreach($questionnaire->questions as $question)
                    @foreach($question->answers as $answer)
                        @if($answer->id == $response->answer_id)
                            {{ $answer->answer }}
                        @endif
                    @endforeach
                @endforeach
            @endforeach
        @endforeach
    </div>--}}

    <table>

            @foreach($questionnaire->questions as $question)
                    <!--Question Code-->
                <tr>
                    <th>Question:</th>
                    <th>{{ $question->text }}</th>
                </tr>


            <!--Answer Code-->
                <th>Answers Given:</th>
                <td></td>
            @foreach($question->responses as $response)
                @foreach($questionnaire->questions as $question)
                    @foreach($question->answers as $answer)
                        @if($answer->id == $response->answer_id)

                            <td>{{ $answer->answer }}</td>
                        @endif
                    @endforeach
                @endforeach
            @endforeach
            @endforeach
    </table>
    <div class="large-6 small-6 columns">
        <a href="/home"><button class="button success round">back to Questionnaire List</button></a>
    </div>


</body>
</html>