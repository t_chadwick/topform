@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="/css/app.css" />
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Your Account</div>

                <div class="panel-body">
                    <div class="row account-content">
                        <div class="large-12 columns">
                            <h4 class="heading" >Your Questionnaires</h4>
                            @if (isset ($questionnaires))
                                    @foreach ($user->questionnaires as $questionnaire)
                                        <div class="row account-questionnaire">
                                            <div class="large-6 columns">
                                                <a href="/questionnaires/{{ $questionnaire->id }}/edit"><p>{{ $questionnaire->title }}</p></a>
                                            </div>
                                            <div class="large-6 columns">
                                                <a href="responses/{{ $questionnaire->id }}/show"><button class="heading button round success tiny"> View Responses</button></a>
                                                <a href="/questionnaires/{{ $questionnaire->id }}/edit"><button class="heading button round info tiny"> Edit</button></a>
                                            </div>
                                        </div>
                                        @if($user->questionnaires == null)
                                        <p>No Questionnaires Added Yet!</p>
                                        @endif
                                    @endforeach
                            @endif
                        </div>
                        <div class="large-12 columns">
                            <a href="questionnaires/create"><button class="heading button round alt-button tiny"> Create New Questionnaire</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
