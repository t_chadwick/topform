<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit - Question</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
    <h1>Edit Question</h1>
    {!! Form::model($question, ['method' => 'PATCH', 'url' => '/questions/'. $question->id]) !!}

    <div>
        {!! Form::label('text', 'Question: ') !!}
        {!! Form::text('text', null) !!}
    </div>

    <div>
        {!! Form::hidden('id['.$id.']') !!}
    </div>

    <div class="large-4 columns">
        {!! Form::submit('Update Question', ['class' => 'button small round edit-button']) !!}
    </div>
    {!! Form::close() !!}
    <div class="large-6 small-6 columns">
        {!! Form::open(['method' => 'DELETE', 'action' => ['QuestionController@destroy', $question->id]]) !!}
        {!! Form::submit('Delete Question', ['class' => 'button right alert small radius']) !!}
        {!! Form::close() !!}
    </div>
</body>
</html>