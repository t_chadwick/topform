@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="/css/app.css" />
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                  Intro
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Live Questionnaires</div>

                <div class="panel-body">
                    <div>
                        Intro
                    </div>
                    <div>
                        <ul style="list-style: none;">
                            @if (isset ($questionnaires))
                                @foreach($questionnaires as $questionnaire)
                                    <div class="large-12 columns"><p></p></div>
                                <div class="account-questionnaire">
                                    <a href="/questionnaires/{{ $questionnaire->id }}"><li>{{ $questionnaire->title }}</li></a>
                                </div>
                                    <div class="large-12 columns"><p></p></div>
                                @endforeach
                            @endif
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
