<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('create a new questionnaire');

// Log in as Admin Dev User
Auth::loginUsingId(1);

// When
$I->amOnPage('/home');
$I->see('TopForm');
$I->see('Your Account');
$I->click('Create New Questionnaire');

// Then
$I->amOnPage('/questionnaires/create');
$I->see('New Questionnaire');
$I->submitForm('#createquestionnaire', [
    'title' => 'Test Questionnaire'
]);

// Then
$I->seeCurrentUrlEquals('/home');
$I->see('Test Questionnaire');