<?php
$I = new FunctionalTester($scenario);

$I->am('user');
$I->wantTo('Fill in a questionnaire');

// Add Database Records of Questionnaire/Question/Answers

// create dummy questionnaire
$I->haveRecord('questionnaires', [
    'id' => '9999',
    'title' => 'Test Questionnaire',
]);
// create dummy question
$I->haveRecord('questions', [
    'id' => '9998',
    'text' => 'Will this test work?',
]);

// create dummy answers
$I->haveRecord('answers', [
    'id' => '9997',
    'answer' => 'Yes it will!',
]);
$I->haveRecord('answers', [
    'id' => '9996',
    'answer' => 'No it wont!',
]);
// Populate required pivot tables
$I->haveRecord('question_questionnaire', [
    'questionnaire_id' => '9999',
    'question_id' => '9998',
]);
$I->haveRecord('answer_question', [
    'answer_id' => '9997',
    'question_id' => '9998',
]);
$I->haveRecord('answer_question', [
    'answer_id' => '9996',
    'question_id' => '9998',
]);
$I->haveRecord('questionnaire_user', [
    'questionnaire_id' => '9999',
    'user_id' => '1',
]);

// When
$I->amOnPage('/');
$I->see('TopForm');
$I->see('Welcome');
$I->see('Test Questionnaire');
$I->click('Test Questionnaire');

// Then
$I->amOnPage('/questionnaires/9999/show');
$I->see('Test Questionnaire');
