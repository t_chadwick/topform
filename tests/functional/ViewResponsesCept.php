<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('View responses for a questionnaire');

// Log in as Admin Dev User
Auth::loginUsingId(1);

// Add Database Records for Questionnaire/Question/Answers/Responses and Pivot Tables

// create dummy questionnaire
$I->haveRecord('questionnaires', [
    'id' => '9999',
    'title' => 'Test Questionnaire',
]);
// create dummy question
$I->haveRecord('questions', [
    'id' => '9998',
    'text' => 'Will this test work?',
]);

// create dummy answers
$I->haveRecord('answers', [
    'id' => '9997',
    'answer' => 'Yes it will!',
]);
$I->haveRecord('answers', [
    'id' => '9996',
    'answer' => 'No it wont!',
]);

// create dummy responses
$I->haveRecord('responses', [
    'id' => '9995',
    'answer_id' => '9997',
]);
$I->haveRecord('responses', [
    'id' => '9994',
    'answer_id' => '9996',
]);

// Populate required pivot tables
$I->haveRecord('question_questionnaire', [
    'questionnaire_id' => '9999',
    'question_id' => '9998',
]);
$I->haveRecord('answer_question', [
    'answer_id' => '9997',
    'question_id' => '9998',
]);
$I->haveRecord('answer_question', [
    'answer_id' => '9996',
    'question_id' => '9998',
]);
$I->haveRecord('question_response', [
    'question_id' => '9998',
    'response_id' => '9995',
]);
$I->haveRecord('question_response', [
    'question_id' => '9998',
    'response_id' => '9994',
]);
$I->haveRecord('questionnaire_user', [
    'questionnaire_id' => '9999',
    'user_id' => '1',
]);


// When
$I->amOnPage('/home');
$I->see('TopForm');
$I->see('Your Account');
$I->see('Test Questionnaire');
$I->click('View Responses');

// Then
$I->amOnPage('/responses');
$I->see('Responses', 'h1');
$I->see('Choose a Questionnaire');
$I->click('Test Questionnaire');

// Then
$I->amOnPage('/responses/9999/show');
$I->see('Responses', 'h1');
$I->see('Test Questionnaire', 'h2');
$I->see('Will this test work?');
$I->see('Total Responses: 2');
$I->see('Yes it will!: 1');
$I->see('No it wont!: 1');

