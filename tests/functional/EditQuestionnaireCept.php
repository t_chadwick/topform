<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('Edit a questionnaire');

// Log in as Admin Dev User
Auth::loginUsingId(1);

// Add Database Records of Questionnaire/Question/Answers

// create dummy questionnaire
$I->haveRecord('questionnaires', [
    'id' => '9999',
    'title' => 'Test Questionnaire',
]);
// create dummy question
$I->haveRecord('questions', [
    'id' => '9998',
    'text' => 'Will this test work?',
]);

// create dummy answers
$I->haveRecord('answers', [
    'id' => '9997',
    'answer' => 'Yes it will!',
]);
$I->haveRecord('answers', [
    'id' => '9996',
    'answer' => 'No it wont!',
]);
// Populate required pivot tables
$I->haveRecord('question_questionnaire', [
    'questionnaire_id' => '9999',
    'question_id' => '9998',
]);
$I->haveRecord('answer_question', [
    'answer_id' => '9997',
    'question_id' => '9998',
]);
$I->haveRecord('answer_question', [
    'answer_id' => '9996',
    'question_id' => '9998',
]);
$I->haveRecord('questionnaire_user', [
    'questionnaire_id' => '9999',
    'user_id' => '1',
]);


// When
$I->amOnPage('/home');
$I->see('TopForm');
$I->see('Your Account');
$I->see('Your Questionnaires');
$I->see('Test Questionnaire');
$I->click('Test Questionnaire');

// Then
$I->amOnPage('/questionnaires/9999/edit');
$I->see('Test Questionnaire', 'h1');
$I->see('Will this test work?');
$I->see('Yes it will!');
$I->see('No it wont!');
$I->click('No it wont!');

// Then
$I->amOnPage('/answers/9996/edit');
$I->see('Edit Answer');
$I->seeInField(['name' => 'answer'], 'No it wont!');
$I->fillField(['name' => 'answer'], 'No it will not!');
$I->click('Update Answer');

// Then
$I->amOnPage('/questionnaires/9999/edit');
$I->see('Test Questionnaire', 'h1');
$I->see('Will this test work?');
$I->see('Yes it will!');
$I->see('No it will not!');
