<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('Delete a questionnaires');

// create 3 dummy questionnaires
$I->haveRecord('questionnaires', [
    'id' => '9999',
    'title' => 'questionnaire1',
]);

// Check that the new records exist
$I->seeRecord('questionnaires', ['title' => 'questionnaire1', 'id' => '9999']);

// Log in as Admin Dev User
Auth::loginUsingId(1);

// When
$I->amOnPage('/admin');
$I->see('TopForm Admin', 'h1');
$I->click('Questionnaires');

// Then
$I->amOnpage('/admin/questionnaires');
$I->see('Questionnaires', 'h1');
$I->see('questionnaire1');
$I->click('questionnaire1');

// Then
$I->amOnPage('/admin/questionnaires/9999/edit');
$I->see('questionnaire1', 'h1');
$I->click('Delete Questionnaire');

// Then
$I->amOnPage('/admin/questionnaires');
$I->dontSee('questionnaire1');