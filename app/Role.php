<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/*
 *  Assign Relationships to Roles
 *  -   Relationship between Roles & Permissions
 */
class Role extends Model
{
    public function permissions() {
        return $this->belongsToMany(Permission::class);
    }

    public function givePermissionTo(Permission $permission) {
        return $this->permission()->sync($permission);
    }
}
