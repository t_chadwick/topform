<?php

namespace App\Http\Controllers;
use App\Question;
use App\Questionnaire;
use Illuminate\Http\Request;

use App\Http\Requests;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $questionnaire = Questionnaire::findOrFail($id);
        $questionnaire_id = $questionnaire->id;
        //print($id);
        //print($questionnaire_id);
        return view('questions/create')->with('questionnaire_id', $questionnaire_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $question = Question::create($request->all());
        $cr_id = ($request->input('id'));
        foreach($cr_id as $inc => $value){
            $question->questionnaire()->attach($inc);
        }
        $question->save();
        foreach($cr_id as $key => $value){
            return redirect('/questionnaires/' . $key . '/edit');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //get the answer
        $question = Question::where('id', $id)->first();

        if(!$question)
        {
            return redirect('/home');
        }
        return view('questions/edit')->with('question', $question)->with('id', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $question = Question::findOrFail($id);
        $question->update($request->all());
        $question->save();
        $up_id = ($request->input('id'));
        foreach($up_id as $icr => $value){
            return redirect('/questionnaires/' . $icr . '/edit');
        }
       // return redirect('/questionnaires/' . $questionnaire . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);

        $question->delete();

            return redirect('/home');
    }
}
