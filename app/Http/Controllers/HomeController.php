<?php

namespace App\Http\Controllers;
use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Questionnaire;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get User
        $user = Auth::user();
        // Get Questionnaires
        $questionnaire = Questionnaire::all();
        return view('home')->with('questionnaires', $questionnaire)->with('user', $user);
    }
}
