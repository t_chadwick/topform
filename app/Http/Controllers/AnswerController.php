<?php

namespace App\Http\Controllers;
use App\Answer;
use App\Question;
use App\Questionnaire;
use Illuminate\Http\Request;

use App\Http\Requests;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id, $id2)
    {
        $questionnaire = Questionnaire::findOrFail($id);
        $questionnaire_id = $questionnaire->id;
        $question = Question::findOrFail($id2);
        $question_id = $question->id;

        return view('answers/create')->with('questionnaire_id', $questionnaire_id)->with('question_id', $question_id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $answer = Answer::create($request->all());
        $cr_id = ($request->input('id'));
        $cr_id2 = ($request->input('id2'));
        foreach($cr_id2 as $inc => $value){
            $answer->question()->attach($inc);
        }
        $answer->save();
        foreach($cr_id as $key => $value){
            return redirect('/questionnaires/' . $key . '/edit');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //get the answer
        $answer = Answer::where('id', $id)->first();

        if(!$answer)
        {
            return redirect('/home');
        }
        return view('answers/edit')->with('answer', $answer)->with('id', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $answer = Answer::findOrFail($id);
        $answer->update($request->all());
        $answer->save();
        $id = ($request->input('id'));
        foreach($id as $key => $value){
            return redirect('/questionnaires/' . $key . '/edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $answer = Answer::find($id);

        $answer->delete();

        return redirect('/home');
    }
}
