<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
 *  Assign Relationships to Permissions
 *  -   Relationship between Permissions & Roles
 */
class Permission extends Model
{
    public function roles() {
        return $this->belongsToMany(Role::class);
    }
}
